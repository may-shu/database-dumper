package com.paras.util.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.paras.util.base.StoredProcedure;

/**
 * Parser that will parse dump file.
 * It will maintain a list of stored procedures after parsing is complete.
 * @author Paras
 *
 */
public class DumpParser{
	
	private static Logger LOGGER = Logger.getLogger( DumpParser.class );
	
	/**
	 * String that marks beginning of a procedure.
	 */
	private static final String PROC_DECLARATION_START = "DELIMITER ;;";
	
	/**
	 * String that marks end of a procedure.
	 */
	private static final String PROC_DECLARATION_END = "DELIMITER ;";
	
	/**
	 * Dump file to parse.
	 */
	private File dump;
	
	/**
	 * List of StoredProcedures encountered.
	 */
	private List<StoredProcedure> procs;
	
	public List<StoredProcedure> getProcs() {
		return procs;
	}

	/**
	 * Create a new DumpParser
	 */
	public DumpParser( String path ) {
		this.dump = new File( path );
	}
	
	/**
	 * Create a new DumpParser
	 */
	public DumpParser( File dump ) {
		this.dump = dump;
	}
	
	/**
	 * Parse Dump.
	 */
	public void parse() {
		LOGGER.info( "DumpParser | Parsing dump at " + dump.getAbsolutePath());
		
		try{
			BufferedReader reader = new BufferedReader( new FileReader( dump ));
			String currentLine = null;
			
			while( (currentLine = reader.readLine()) != null ) {
				
				if( currentLine.startsWith( PROC_DECLARATION_START )) {
					
					StringBuilder builder = new StringBuilder();
					String name = null;
					
					while( !(currentLine = reader.readLine()).equals( PROC_DECLARATION_END)) {
						
						String line = currentLine;
						
						if( builder.length() == 0 ) {
							name = getName( line );
							LOGGER.info( "DumpParser | Encountered Routine with name : " + name );
						}
						builder.append( currentLine );
						builder.append("\n");
					}
					
					StoredProcedure procedure = new StoredProcedure( name, builder.toString());
					
					if( procs == null ) {
						procs = new ArrayList<StoredProcedure>();
					}
					
					procs.add( procedure );
				}
				
			}
			
			reader.close();
			LOGGER.info( "DumpParser | Parsing Complete | Found " + procs.size() + " routines.");
		}
		catch( IOException ex ) {
			LOGGER.error( "DumpParser | Caught IOException | " + ex.getMessage());
			LOGGER.debug( ex );
		}
	}
	
	private String getName( String line ) {
		String definition = StringUtils.substringBefore( line, "(");
		String literal = null;
		
		if( definition.contains( " FUNCTION ")) {
			literal = StringUtils.substringAfter( definition, "FUNCTION " ); 
		} else {
			literal = StringUtils.substringAfter( definition, "PROCEDURE " ); 
		}
		
		literal = literal.trim().replaceAll( "`", "");
		
		return literal;
	}
}