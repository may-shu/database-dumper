package com.paras.util.mysql;

import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * Dumper class to dump mysql content.
 * @author Paras
 *
 */
public class Dumper {
	
	private static Logger LOGGER = Logger.getLogger( Dumper.class );

	/**
	 * Database name for which dump is to be created.
	 */
	private String dbName;
	
	/**
	 * User name which will be used to retrieve dump.
	 */	
	private String user;
	
	/**
	 * User's password.
	 */
	private String password;
	
	/**
	 * Dump's location.
	 */
	private String dump;
	
	/**
	 * Create a dumper instance.
	 * @param dbName
	 * @param user
	 * @param password
	 */
	public Dumper( String dbName, String user, String password ) {
		this.dbName = dbName;
		this.user = user;
		this.password = password;
	}
	
	/**
	 * Create a dumper instance.
	 */
	public Dumper( String dbName, String user, String password, String dump ) {
		this.dbName = dbName;
		this.user = user;
		this.password = password;
		this.dump = dump;
	}
	
	/**
	 * Create dump.
	 * @return
	 */
	public boolean create() {
		LOGGER.info( "Dumper | Creating dump of db " + dbName );
		
		String command = getCommand();
		LOGGER.debug( "Dumper | Command to execute | " + command );
		
		boolean dumpCreated = run( command );
		
		if( dumpCreated ) {
			LOGGER.info( "Dumper | Dump Creation finished.");		
			
		} else {
			LOGGER.info( "Dumper | Dump Creation Failed.");
		}		
		
		return dumpCreated;
	}
	
	/**
	 * Private method to create command that will generate dump.
	 */
	private String getCommand() {
		String command = "mysqldump -u " + user + " -p" + password + " --databases " + dbName + " -r " + dump + " --no-create-info --no-data --no-create-db --routines";
		return command;
	}
	
	/**
	 * Execute command in run time.
	 */
	public boolean run( String command ) {
		LOGGER.info( "Dumper | Executing Command" );
		boolean result = false;
		
		try {
			
			Process process = Runtime.getRuntime().exec( command );
			int processReturnValue = process.waitFor();
			
			if( processReturnValue == 0 ) {
				result = true;
			}
			
		} catch (IOException ex) {
			
			LOGGER.error( "Dumper | Caught IOException while executing command | " + ex.getMessage());
			LOGGER.debug( ex );
			
		} catch (InterruptedException ex ) {
			
			LOGGER.error( "Dumper | Caught InterruptedException while executing command | " + ex.getMessage());
			LOGGER.debug( ex );
		}
		
		return result;
		
	}
}
