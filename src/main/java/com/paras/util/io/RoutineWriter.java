package com.paras.util.io;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.paras.util.base.StoredProcedure;

/**
 * Writer that will write routine to file system.
 * @author Paras
 *
 */
public class RoutineWriter {
	
	private static Logger LOGGER = Logger.getLogger( RoutineWriter.class );

	/**
	 * Working directory.
	 */
	private String workingDirectory;
	
	public RoutineWriter( String dir ) {
		this.workingDirectory = dir;
	}
	
	public void write( StoredProcedure procedure ) {
		
		String name = this.workingDirectory + File.separator + "Routines" + File.separator + procedure.getName() + ".sql";
		
		try{

			LOGGER.info("RoutineWriter | Writing body of " + procedure.getName() + " to " + name );
			FileUtils.writeStringToFile(new File( name ), procedure.getBody());
			
		}
		catch( IOException ex ) {
			LOGGER.error( "RoutineWriter | Caught IOException | " + ex.getMessage());
		}
		
	}
	
	public void writeAll( List<StoredProcedure> procs ) {
		for( StoredProcedure proc : procs ) {
			write( proc );
		}
	}
	
}
