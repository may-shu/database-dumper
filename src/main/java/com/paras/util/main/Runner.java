package com.paras.util.main;

import java.io.File;
import java.util.List;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.log4j.Logger;

import com.paras.util.base.StoredProcedure;
import com.paras.util.io.RoutineWriter;
import com.paras.util.mysql.Dumper;
import com.paras.util.parser.DumpParser;

/**
 * This class will kick start application.
 * @author Paras
 *
 */
public class Runner {

	private static Logger LOGGER = Logger.getLogger( Runner.class );
	
	public static void main( String ... args ) {
		LOGGER.info( "Runner | Starting Execution");
		
		String db, user, password, dump, workingDirectory;
		workingDirectory= db = user = password = dump = null;
		
		boolean allParamsPresent = true;
		
		Options options = new Options();		
		options.addOption( "d", true, "Database Name");		
		options.addOption( "u", true, "User Name" );
		options.addOption( "p", true, "Password" );
		options.addOption( "o", true, "Dump file");
		options.addOption( "w", true, "Working Directory");
		
		CommandLineParser parser = new DefaultParser();
		
		try {
			
			CommandLine command = parser.parse( options, args );
			
			if( command.hasOption( "d" )) {
				db = command.getOptionValue( "d" );
				LOGGER.info( "Runner | Will use " + db + " for dumping.");				
			} else {
				LOGGER.error( "Runner | No database provided for dumping.");
				allParamsPresent = false;
			}
			
			if( command.hasOption( "u" )) {
				user = command.getOptionValue( "u" );
				LOGGER.info( "Runner | Will use " + user + " as database user.");				
			} else {
				LOGGER.error( "Runner | No user provided for dumping.");
				allParamsPresent = false;
			}
			
			if( command.hasOption( "p")) {
				password = command.getOptionValue( "p" );
				LOGGER.info( "Runner | Will use " + password + " as database password.");
			} else {
				LOGGER.error( "Runner | No password provided for dumping");
				allParamsPresent = false;
			}
			
			if( command.hasOption( "w" )) {
				workingDirectory = command.getOptionValue( "w" );
				LOGGER.info( "Runner | Will use " + workingDirectory + " as working directory..");
			} else {
				LOGGER.info( "Runner | Will use current directory as working directory.");
			}
			
			if( command.hasOption( "o")) {
				dump = command.getOptionValue( "o" );
				
				if( null != workingDirectory ) {
					dump = workingDirectory + File.separator + dump;
				}
				
				LOGGER.info( "Runner | Will use " + dump + " as dump file." );
			} else {
				LOGGER.error( "Runner | No dump file specified.");
				allParamsPresent = false;
			}
			
			if( allParamsPresent ) {
				
				Dumper dumper = new Dumper( db, user, password, dump );				
				boolean result = dumper.create();
				
				if( result ) {
					LOGGER.info( "Runner | Dump has been created successfully at location " + dump );
					
					DumpParser dumpParser = new DumpParser( dump );
					dumpParser.parse();
					
					List<StoredProcedure> routines = dumpParser.getProcs();
					RoutineWriter writer = new RoutineWriter( workingDirectory );
					
					writer.writeAll( routines );
					
					LOGGER.info( "Runner | All Tasks Completed.");
					
				} else {
					LOGGER.info( "Runner | No dump created | Program will exit now.");
				}
				
			} else {
				LOGGER.error( "Runner | One of required parameters to process dump is missing. Program will exit now.");
			}
			
		} catch (ParseException ex) {
			LOGGER.error( "Runner | Caught ParseException | " + ex.getMessage());
		}
	}
	
}
