package com.paras.util.base;

/**
 * A Stored Procedure in real world.
 * @author Paras
 *
 */
public class StoredProcedure {

	/**
	 * Name of stored procedure.
	 */
	private String name;
	
	/**
	 * Body of stored procedure.
	 */
	private String body;
	
	/**
	 * File where it's body needs to be dumped.
	 */
	private String path;
	
	/**
	 * Create a stored procedure.
	 * @param name
	 * @param body
	 */
	public StoredProcedure( String name, String body ) {
		this.name = name;
		this.body = body;
	}
	
	/**
	 * Create a stored procedure.
	 */
	public StoredProcedure( String name, String body, String path ) {
		this.name = name;
		this.body = body;
		this.path = path;
	}
	
	public void setPath( String path ) {
		this.path = path;
	}
	
	public String getPath() {
		return path;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getBody() {
		return this.body;
	}
	
	@Override
	public String toString() {
		return name + "\n" + body;
	}
	
}
